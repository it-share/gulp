/*
	Name: count search
	Feature: show the result job number when checked, selected, typed, closed modal
*/
import $ from 'jquery'
/* 
    edited at 2016-11-08 by Bui Huy Binh
    reason:  bring __makeUrl and __count out outside in order to be able to call in app.js
*/
var __makeUrl = function (obj) {

	//checkboxs
	var uri = [];
	var textboxKeyword = $('#search_keyword');
	var selectProvince = $('#pro_select');

	if (textboxKeyword.val() != '') {
		uri.push('k=' + encodeURIComponent(textboxKeyword.val()));
	}

	if (selectProvince.val() != '') {
		uri.push('pro=' + selectProvince.val());
	}

	$('.search-checkbox:checked').each(function () {
		var _this = $(this);
		var _name = _this.attr('name');
		if (obj.id == 'pro_select' &&
			(_name == 'city[]' || _name == 'way[]')
		) {
			return;
		}
		uri.push(encodeURIComponent(_name) + '=' + _this.val());
	});

	var url = '/job/ajax-count-search?' + uri.join('&');

	return url;
}

/*Count function*/
var __count = function (obj) {

	var url = __makeUrl(obj);

	$('#btnAdvanceSubmit').html('...件を検索');
	$.get(url, function (numberResult) {
		$('#btnAdvanceSubmit').html(numberResult + '件を検索');
	});
}


var count_search = function () {
	/*Events*/
	var eventFunc = function (obj, action) {
		$(obj).on(action, function () {
			__count(obj);
		});
	};

	//When done press keyword.
	$('#search_keyword').length &&
		$('#search_keyword').on("blur", function () {
			__count(this);
		});


	//When select city checkbox or waysite checkbox
	$(window).on("change", function (e) {
		var target = e.target;
		if (target.className == "search-checkbox city-checkbox" ||
			target.className == "search-checkbox waysite-checkbox") {
			__count(target);
		}
	});



	//When select 都道府県
	$('#pro_select').length && eventFunc($('#pro_select')[0], 'change');

	//When checkbox 職種, 雇用形態, こだわり
	var checkboxes = $('.search-checkbox');
	var cLeng = checkboxes.length;
	for (var c = 0; c < cLeng; c++) {
		if (checkboxes[c].name == 'em[]' ||
			checkboxes[c].name == 'cat[]' ||
			checkboxes[c].name == 'hang[]') {
			eventFunc(checkboxes[c], 'change');
		}
	}

	//When close City or Waysite modal
	var closeModalButtons = $('.close_modal_button');
	var bLeng = closeModalButtons.length;
	for (var b = 0; b < bLeng; b++) {
		eventFunc(closeModalButtons[b], 'click');
	}
}

export default count_search;


/*Count function*/
export function realTimeSearch(obj) {
    __count(obj);
};