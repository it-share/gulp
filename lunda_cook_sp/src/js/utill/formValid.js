import $ from 'jquery'
import setTimeOut from './settimeout'

//例data-error="mail"を持ったエラーdomとdata-mailをもったinputでshowhide部分が有効に
//特定data属性を持っていたら有効にするようカスタマイズが必要
//maxlength,minlength はdom側で数値を指定する
//this.attrs内で指定すると全てのinputにかかってしまうため

class ValidateModel{
	constructor(attrs){
		this.val = "";
		this.attrs = {
			required     : attrs.required     || false,
			mail         : attrs.mail         || false,
			kana         : attrs.kana         || false,
			num          : attrs.num          || false,
			alphanumeric : attrs.alphanumeric || false,
			maxlength    : attrs.maxlength    || false,
			minlength    : attrs.minlength    || false
		};
		this.listeners = {
			valid: [],
			invalid: []
		};
	}
	set(val) {
		if (this.val === val) return;
		this.val = val;
		this.validate();
	}
	validate() {
		var value;
		this.errors = [];
		for (var key in this.attrs) {
			//constructor this.attrsのkeyが呼ばれる
			value = this.attrs[key];
			//this[required] this.requiredが呼ばれる ここのせいでmaxlengthとminlengthがすべてにあたってしまう。修正が必須
			//ただ下記を消すとerrorが呼べなくなる
			//maxlength(num)numを切るとぶっ壊れる
			// this.maxlength = 8;　これのせいか
			// console.log(this[key](value) + "bbb");
			if (value && !this[key](value)) this.errors.push(key);
		}
		this.trigger(!this.errors.length ? "valid" : "invalid");
	}
	on(event, func) {
		this.listeners[event].push(func);
		//this.listenersのkey内配列にpush
	}
	trigger(event) {
		$.each(this.listeners[event], function() {
			this();
		});
	}
	required() {
		return this.val !== "";
	}
	mail() {
		return this.val.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) != null;
	}
	kana() {
		return this.val.match(/^([ァ-ヶー]+)$/) != null;
	}
	num() {
		return this.val.match(/^([0-9])+$/) != null;
	}
	alphanumeric() {
		return this.val.match(/^([A-Za-z0-9]+$)/) != null;
	}
	maxlength(num) {
		return num >= this.val.length;
	}
	minlength(num) {
		return num <= this.val.length;
	}
}


//下記は別fileにしたほうが疎結合かな
export class ValidateView{
	constructor(el){
		this.init(el);
		this.handleEvents();
		this.loadEvents();
	}
	init(el) {
		this.$el = $(el);
		this.$list = this.$el.prev().children();
		var obj = this.$el.data();
		if (this.$el.prop("required")) {
			obj["required"] = true;
		}
		// console.log(obj);
		/*
		 obj : dom情報を持っている 下記例
		 Object {validate: "target", mail: "target", required: true}
		*/
		this.model = new ValidateModel(obj);
		//this.model ValidateModel {val: "", attrs: Object, listeners: Object}
		//validate: "target"を持っているdom分インスタンス作成
	}
	handleEvents() {
		//$elにon
		this.$el.on("input", (e) => {
			this.onKeyup(e);
		});
		//まずmodelにvalidをbind
		this.model.on("valid", () => {
			this.onValid();
		});

		//まずmodelにvalidをbind
		this.model.on("invalid", () => {
			this.onInvalid();
		});
	}
	loadEvents() {
		if(this.$el.val().length > 0) {
			this.onValid();
		}
	}
	onKeyup(e) {
		var $target = $(e.currentTarget);
		this.model.set($target.val());
	}
	onValid() {
		this.$el.removeClass("error").addClass("valid");
		this.$list.hide();
	}
	onInvalid() {
		this.$el.addClass("error").removeClass("valid");
		this.$list.hide();
		//下記=>でthis渡し
		$.each(this.model.errors, (index, val) => {
			this.$list.filter(`[data-error=${val}]`).show();
		});
	}
	multipleValid(t,tag){
		var $targetWrap = t,
			$error  = $targetWrap.find('[data-error-multi]'),
			$target = $targetWrap.find(tag),
			validFlg = false;
		$target.on('change',function(){
			$target.each(function(){
				if($(this).val() == ""){
					// $error.show();
					return;
				}
				validFlg = true;
				$error.hide();
			});
		});
	}
	onSubmitMultipleValid(t,tag) {
		var $targetWrap = t,
			$error  = $targetWrap.find('[data-error-multi]'),
			$target = $targetWrap.find(tag),
			validFlg = false;
		$target.each(function(){
			if($(this).val() == ""){
				$error.show();
				return false;
			}
			validFlg = true;
			$error.hide();
		});
	}
	checkedValid(t){
		var $targetWrap = t,
			$error  = $targetWrap.find('[data-error-check]'),
			$target = $targetWrap.find('input');
		$target.on('change',function() {
			$target.each(function(){
				if($(this).prop('checked') == true ){
					$error.hide();
					return false;
				}
				$error.show();
			});
		});
	}
	submit(t,e){
		var firstErrorFlag = false;
		t.each(function(){
			if($(this).hasClass("valid")) {
			} else {
				e.preventDefault();
				$(this).prev().children().show();
				setTimeOut(1).then(function(){
					$.each($('.form__error'), function(){
						if(($(this).is(':visible')) && (firstErrorFlag == false)) {
							var errorPosition = $(this).offset().top;
							$('html,body').animate({scrollTop: errorPosition - 80}, 400, 'swing');
							firstErrorFlag = true;
						}
					});
				});
			}
		});
	}
	reset(t){
		t.each(function(){
			$(this).val('');
			if($(this).hasClass("valid")) {
				$(this).removeClass("valid");
			}
		});
	}
}

export class ValidateViewModal extends ValidateView {
	init(el){
		super.init(el);
		this.$list = this.$el.next().children();
	}
	submit(t,e) {
		t.each(function(){
			if ($(this).hasClass("valid")) {
			}else{
				e.preventDefault();
				$(this).next().children().show();
			}
		});
	}
}

