const paths = {
	js:         'src/js/*.js',
	js_utill:    'src/js/utill/*.js',
	jade: ['src/jade/*.jade','src/jade/**/*.jade','!' + 'src/jade/partial/_*.jade','!' + 'src/jade/partial/**/_*.jade'],
	jade_watch: ['src/jade/*.jade','src/jade/**/*.jade'],
	html_build: 'build/',
	stylus:       'src/stylus/*.styl',
	stylus_watch: ['src/stylus/*.styl','src/stylus/**/*.styl','src/stylus/**/**/*.styl'],
	stylus_dir: 'src/stylus',
	stylus_sprite: 'src/stylus/object/component/sprite/',
	css_dir:    'dist/css',
	css_dist:   'dist/css/*.css',
	css_build:  'build/css',
	dist:       'dist/**/*',
	dist_dir:   'dist',
	js_dir:     'dist/js',
	js_dist:    'dist/*.js',
	js_bundle:  'src/js/lib/*.js',
	img:        ['src/img/*','src/img/**/*'],
	img_dist:   'dist/img',
	img_build:  'build/img',
	html_dist:  'dist/*.html',
	build:      'build'
};

export default paths;










