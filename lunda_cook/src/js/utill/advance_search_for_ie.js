/*
	Name: advance_search for IE
	Feature: use on IE, because it's not support 'form' attribute HTML5
*/
import $ from 'jquery'
var advance_search_for_ie = function(){


        /******START Advance search ie JS
         * date 18/Oct/2016
         * advence searcch using on IE
         * update by Trinh Manh Cuong
         * ****/

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
			
			//When IE
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer
            {

                /*Get values to make URL*/
                var __makeUrl = function () {

                    //checkboxs
                    var uri = [];
                    var textboxKeyword = $('#search_keyword');
                    var selectProvince = $('#pro_select');
                    var checkboxes = $(".search-checkbox");

                    if(textboxKeyword.val() != '')
                    {
                        uri.push('k='+encodeURIComponent(textboxKeyword.val()));
                    }

                    if(selectProvince.val() != '')
                    {
                        uri.push('pro='+selectProvince.val());
                    }
					
					$(".search-checkbox:checked").each(function () {
						uri.push(encodeURIComponent($(this).attr('name'))+'='+$(this).val());
					})
					
             

                    var url = '/job/result?'+uri.join('&');
                    return url;
                }
		
                document.forms["searchForm"].onsubmit = function(){
                    var url = __makeUrl();
                    window.location.href = url;
                    return false;
                }
            }
        /******END  Advance search ie JS****/
}

export default advance_search_for_ie;